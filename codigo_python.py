import serial
import time
from datetime import datetime

# Configuración del puerto serie (ajusta el puerto según tu entorno)
arduino_port = '/dev/ttyUSB0'
baud_rate = 9600

# Abre el puerto serie
ser = serial.Serial(arduino_port, baud_rate, timeout=1)

# Función para leer datos desde Arduino
def read_arduino_data():
    data = ser.readline().decode('utf-8').strip()
    return data

# Función para guardar datos en el archivo de bitácora
def log_data_to_file(log_string):
    with open('bitacora.txt', 'a') as log_file:
        log_file.write(log_string + '\n')

# Bucle principal
try:
    while True:
        # Leer datos desde Arduino
        arduino_data = read_arduino_data()

        # Obtener la fecha y hora actual
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")

        # Crear la cadena de la bitácora
        log_string = f"{formatted_datetime} | {arduino_data}"
	
	    # Visualizar 
        print(f"Guardando lectura {log_string}")
        # Guardar datos en el archivo de bitácora
        log_data_to_file(log_string)

        # Esperar un tiempo antes de la próxima lectura
        time.sleep(1)

except KeyboardInterrupt:
    # Cerrar el puerto serie al salir del bucle
    ser.close()
    print("Programa interrumpido. Puerto serie cerrado.")
