#include <LiquidCrystal_I2C.h>
#include <DHT.h>
#include <SoftwareSerial.h>

SoftwareSerial BT(11, 12);

#define TRIG_PIN 9
#define ECHO_PIN 8
#define MOV_IZQ_PIN 2
#define MOV_DER_PIN 4
#define DHTPIN 10
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);
LiquidCrystal_I2C lcd(0x27, 16, 2);

float filterArray[20];
float distance;

int movIzqPin = MOV_IZQ_PIN;
int movDerPin = MOV_DER_PIN;

bool motorEncendidoFlag = false;

bool motorEncendidoFlag2 = false;

float temperatura;

void setup() {
  lcd.backlight();
  Serial.begin(9600);
  BT.begin(9600);
  lcd.begin(16, 2);

  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(movIzqPin, OUTPUT);
  pinMode(movDerPin, OUTPUT);

  dht.begin();
}

void loop() {
  // Medir la temperatura
  temperatura = dht.readTemperature();
  Serial.print("Temperatura: ");
  Serial.print(temperatura);
  Serial.print(" C |");

  // Mostrar la temperatura en el LCD
  mostrarTemperaturaLCD();

  medirDistancia();
  moduloBluetooh();
  hayTemperaturaAltaoEstoyCerca();
  accionMotor();

  delay(1000);
}

void mostrarTemperaturaLCD() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Temp: ");
  lcd.print(temperatura);
  lcd.print("C");
  lcd.setCursor(0, 1);
  lcd.print("Ventilador");
}

void accionMotor() {
  if (motorEncendidoFlag || motorEncendidoFlag2) {
    digitalWrite(movIzqPin, HIGH);
    digitalWrite(movDerPin, LOW);
    Serial.println(" Motor Encendido |");
  } else {
    digitalWrite(movIzqPin, LOW);
    digitalWrite(movDerPin, LOW);
  }
  delay(3000);
}

void moduloBluetooh() {
  if (BT.available()) {
    byte dato = BT.read();
    if (dato == '1') {
      motorEncendidoFlag2 = true;
      lcd.clear();
      lcd.print("Motor Activado");
    } else {
      motorEncendidoFlag2 = false;
      lcd.clear();
      lcd.print("Motor Apagado");
    }
  }
}

void hayTemperaturaAltaoEstoyCerca() {
  
  if (temperatura >= 32 || distance < 15) {
    motorEncendidoFlag = true;
  } else {
    motorEncendidoFlag = false;
  }
}

void medirDistancia() {
  // Generar un pulso ultrasónico para activar el sensor
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // Medir el tiempo de vuelo del pulso ultrasónico
  unsigned long tiempoVuelo = pulseIn(ECHO_PIN, HIGH);

  // Calcular la distancia en centímetros
  distance = tiempoVuelo * 0.034 / 2;  // La velocidad del sonido es de aproximadamente 0.034 cm/microsegundo

  // Imprimir la distancia en el monitor serial
  Serial.print(" Distancia: ");
  Serial.print(distance);
  Serial.println(" cm");
}
