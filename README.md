# Proyecto para el Ramo Introducción a la Computación Física con Arduino 2023 UBB

## Creado por José Arriagada

Descripción del Proyecto 

**Problematica:** Casas pequeña,verano caluroso, y sin sistema de refrigeración.

**Objetivo General:** Crear un proyecto con Arduino que sea capaz de bajar la temperatura.

**Objetivo Específico:** Poder tener un historial de la temperatura de la casa.

### Lista de materiales:

- **Arduino Mega 2560**

- **LCD I2C**

- **Sensor DHT** (Para la temperatura)

- **Sensor HC-SR04** (Para detectar proximidad)

- **Módulo Bluetooth HC-05**

- **L293D** (Para controlar el Motor)

- **Motor DC**

- **Módulo de Alimentación**

- **Batería 9V**

- **Caja pila AA**


#### Diagrama del Proyectos

![Diagrama](presentación/img/diagrama_arduino.jpeg)

 ### Descripción del Proyecto

 Este proyecto es desarrollado en Arduino, para controlar el arduino y el hardware; y además de utilizo python, con su librería pyserial, para implementar la bitácora.

### El Proyecto hace los Siguiente

-Se lee la consola por conexión por el pueto Seriel y se recolectan los datos de esta, esta tiene los valores de la temperatura y el valor del sensor de ultrasonido Con un código python se guardan los valores en un archivo txt.

-Por Bluetooth se puede encender el ventilador.

-Al acercarse se enciende el ventilador si la temperatura está a una distancia de 15 cm, y cuando la temperatura está 32°C.

-Y el LCD muestra la temperatura actual.

**Resultado**

![Imagen conexiones](presentación/img/resultado.jpg)

